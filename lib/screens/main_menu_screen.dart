import 'package:flutter/material.dart';
import 'package:flutter_game/responsive/responsive.dart';
import 'package:flutter_game/screens/create_room_screen.dart';
import 'package:flutter_game/screens/join_room_screen.dart';
import 'package:flutter_game/widgets/custom_button.dart';
import 'package:flutter_game/widgets/custom_text.dart';
import 'package:flutter_svg/svg.dart';

class MainMenuScreen extends StatelessWidget {
  static String routeName = '/main-menu';
  const MainMenuScreen({super.key});

  void createRoom(BuildContext context) {
    Navigator.pushNamed(context, CreateRoomScreen.routeName);
  }

  void joinRoom(BuildContext context) {
    Navigator.pushNamed(context, JoinRoomScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Responsive(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const CustomText(
              shadows: [
                Shadow(
                  blurRadius: 40,
                  color: Colors.blue,
                ),
              ],
              text: 'Tic Tac Toe',
              fontSize: 70,
            ),
            SvgPicture.network(
              'https://api.dicebear.com/9.x/fun-emoji/svg?radius=50&seed=flutter_game',
            ),
            const SizedBox(height: 20),
            CustomButton(
              onPressed: () => createRoom(context),
              text: 'Create Room',
            ),
            const SizedBox(height: 20),
            CustomButton(
              onPressed: () => joinRoom(context),
              text: 'Join Room',
            ),
          ],
        ),
      ),
    );
  }
}
