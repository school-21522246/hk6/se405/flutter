import 'package:flutter/material.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:flutter_game/resources/socket_methods.dart';
import 'package:flutter_game/responsive/responsive.dart';
import 'package:flutter_game/widgets/custom_button.dart';
import 'package:flutter_game/widgets/custom_text.dart';
import 'package:flutter_game/widgets/custom_textfield.dart';
// ignore: library_prefixes
import 'package:flutter_game/utils//utils.dart' as Utils;
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class JoinRoomScreen extends StatefulWidget {
  static String routeName = '/join-room';
  const JoinRoomScreen({super.key});

  @override
  State<JoinRoomScreen> createState() => _JoinRoomScreenState();
}

class _JoinRoomScreenState extends State<JoinRoomScreen> {
  final TextEditingController _gameIdController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final SocketMethods _socketMethods = SocketMethods();

  @override
  void initState() {
    super.initState();
    _socketMethods.errorOccuredListener(context);
    _socketMethods.updateRoomListener(context);
    _socketMethods.navigateGameScreenListener(context);
  }

  @override
  void dispose() {
    super.dispose();
    _socketMethods.clearListeners();
    _gameIdController.dispose();
    _nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final roomDataProvider = Provider.of<RoomDataProvider>(context, listen: false);
    var icon = 'https://api.dicebear.com/9.x/fun-emoji/svg?radius=50&seed=${_nameController.text}';

    return Scaffold(
      body: Responsive(
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const CustomText(
                shadows: [
                  Shadow(
                    blurRadius: 40,
                    color: Colors.blue,
                  ),
                ],
                text: 'Join Room',
                fontSize: 70,
              ),
              SvgPicture.network(icon),
              SizedBox(height: size.height * 0.08),
              CustomTextField(
                controller: _nameController,
                hintText: 'Enter your nickname',
                onChange: (value) => Utils.debounce(
                  const Duration(milliseconds: 500),
                  (value) => setState(() {
                    icon = 'https://api.dicebear.com/9.x/fun-emoji/svg?radius=50&seed=${_nameController.text}';
                  }),
                  [value]
                ),
              ),
              const SizedBox(height: 20),
              CustomTextField(
                controller: _gameIdController,
                hintText: 'Enter Game ID',
              ),
              SizedBox(height: size.height * 0.045),
              CustomButton(
                onPressed: () => {
                  _socketMethods.joinRoom(_nameController.text, icon, _gameIdController.text),
                  roomDataProvider.setNickname(_nameController.text),
                },
                text: 'Join',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
