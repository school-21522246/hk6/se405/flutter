import 'package:flutter/material.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:flutter_game/resources/socket_methods.dart';
import 'package:flutter_game/responsive/responsive.dart';
import 'package:flutter_game/widgets/custom_button.dart';
import 'package:flutter_game/widgets/custom_text.dart';
import 'package:flutter_game/widgets/custom_textfield.dart';
// ignore: library_prefixes
import 'package:flutter_game/utils//utils.dart' as Utils;
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class CreateRoomScreen extends StatefulWidget {
  static String routeName = '/create-room';
  const CreateRoomScreen({super.key});

  @override
  State<CreateRoomScreen> createState() => _CreateRoomScreenState();
}

class _CreateRoomScreenState extends State<CreateRoomScreen> {
  final TextEditingController _nameController = TextEditingController();
  final SocketMethods _socketMethods = SocketMethods();
  late Size size = MediaQuery.of(context).size;
  late RoomDataProvider roomDataProvider =
      Provider.of<RoomDataProvider>(context, listen: false);
  late String icon = getIcon(_nameController.text);

  @override
  void initState() {
    super.initState();
    _socketMethods.errorOccuredListener(context);
    _socketMethods.updateRoomListener(context);
    _socketMethods.navigateGameScreenListener(context);
  }

  @override
  void dispose() {
    super.dispose();
    _socketMethods.clearListeners();
    _nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Responsive(
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const CustomText(
                shadows: [
                  Shadow(
                    blurRadius: 40,
                    color: Colors.blue,
                  ),
                ],
                text: 'Create Room',
                fontSize: 70,
              ),
              SvgPicture.network(icon),
              SizedBox(height: size.height * 0.08),
              CustomTextField(
                controller: _nameController,
                hintText: 'Enter your Nickname',
                onChange: (value) => Utils.debounce(
                    const Duration(milliseconds: 500),
                    (value) => setState(() {
                          icon = getIcon(_nameController.text);
                        }),
                    [value]),
              ),
              SizedBox(height: size.height * 0.045),
              CustomButton(
                text: 'Create',
                onPressed: () => {
                  _socketMethods.createRoom(_nameController.text, icon),
                  roomDataProvider.setNickname(_nameController.text),
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  String getIcon(String name) {
    return 'https://api.dicebear.com/9.x/fun-emoji/svg?radius=50&seed=$name';
  }
}
