import 'package:flutter/material.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:flutter_game/resources/socket_methods.dart';
import 'package:flutter_game/views/scoreboard.dart';
import 'package:flutter_game/views/tictactoe_board.dart';
import 'package:flutter_game/views/waiting_lobby.dart';
import 'package:provider/provider.dart';

class GameScreen extends StatefulWidget {
  static String routeName = '/game';
  const GameScreen({super.key});

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  final SocketMethods _socketMethods = SocketMethods();
  late RoomDataProvider roomDataProvider = Provider.of<RoomDataProvider>(context);
  late String turnNickname = roomDataProvider.roomData['turn']['nickname'];
  late String myNickname = roomDataProvider.myNickname;
  late bool isMyTurn = turnNickname == myNickname;

  @override
  void initState() {
    super.initState();
    _socketMethods.errorOccuredListener(context);
    _socketMethods.updateRoomListener(context);
    _socketMethods.endGameListener(context);
    _socketMethods.navigateBackListener(context);
    _socketMethods.tappedListener(context);
  }

  @override
  void dispose() {
    super.dispose();
    _socketMethods.clearListeners();
    _socketMethods.leaveRoom(
      roomId: roomDataProvider.roomData['_id'],
      nickname: myNickname,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: roomDataProvider.roomData['canJoin']
          ? const WaitingLobby()
          : SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Scoreboard(),
                  const TicTacToeBoard(),
                  Text(
                    isMyTurn ? 'Your turn' : '$turnNickname\'s turn',
                    style: TextStyle(
                      color: isMyTurn ? Colors.green : Colors.white,
                      fontWeight:
                          isMyTurn ? FontWeight.bold : FontWeight.normal,
                      fontSize: 20,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      _socketMethods.leaveRoom(
                        roomId: roomDataProvider.roomData['_id'],
                        nickname: myNickname,
                      );
                    },
                    child: const Text('Leave'),
                  ),
                ],
              ),
            ),
    );
  }
}
