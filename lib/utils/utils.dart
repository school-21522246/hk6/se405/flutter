import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

void showSnackBar(BuildContext context, String content) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(content),
    ),
  );
}

void showEndGameDialog({context, title, icon, content, onPressed}) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: icon != null
              ? Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SvgPicture.network(
                      icon,
                      height: 100,
                      width: 100,
                    ),
                    const SizedBox(height: 20),
                    Text(content)
                  ],
                )
              : Text(content),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                onPressed();
              },
              child: const Text(
                'Play Again',
              ),
            ),
          ],
        );
      });
}

Map<Function, Timer> _timeouts = {};
void debounce(Duration timeout, Function target, [List arguments = const []]) {
  if (_timeouts.containsKey(target)) {
    _timeouts[target]?.cancel();
  }

  Timer timer = Timer(timeout, () {
    Function.apply(target, arguments);
  });

  _timeouts[target] = timer;
}
