import 'package:flutter/material.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:flutter_game/resources/socket_methods.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class TicTacToeBoard extends StatefulWidget {
  const TicTacToeBoard({super.key});

  @override
  State<TicTacToeBoard> createState() => _TicTacToeBoardState();
}

class _TicTacToeBoardState extends State<TicTacToeBoard> {
  final SocketMethods _socketMethods = SocketMethods();

  @override
  void initState() {
    super.initState();
  }

  void tapped(int index, RoomDataProvider roomDataProvider) {
    if (roomDataProvider.board[index] != '') {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('This cell is already taken'),
        ),
      );
      return;
    }
    _socketMethods.tapGrid(
      index,
      roomDataProvider.roomData['_id'],
      roomDataProvider.board,
    );
  }

  @override
  Widget build(BuildContext context) {
    final contextSize = MediaQuery.of(context).size;
    RoomDataProvider roomDataProvider = Provider.of<RoomDataProvider>(
      context,
    );
    final int size = roomDataProvider.size;

    return Container(
      decoration: const BoxDecoration(
          // boxShadow: [
          //   BoxShadow(
          //     color: Colors.blue,
          //     blurRadius: 5,
          //     spreadRadius: 0,
          //   )
          // ],
          ),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: contextSize.height * 0.58,
          maxWidth: 500,
        ),
        child: AbsorbPointer(
          absorbing: roomDataProvider.roomData['turn']['socketID'] !=
              _socketMethods.socketClient.id,
          child: GridView.builder(
            itemCount: size * size,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: size,
            ),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () => tapped(index, roomDataProvider),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white24,
                    ),
                  ),
                  child: SizedBox(
                    child: Center(
                      child: AnimatedSize(
                        duration: const Duration(milliseconds: 200),
                        child: roomDataProvider.board[index].isEmpty
                            ? null
                            : SvgPicture.network(roomDataProvider.board[index]),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
