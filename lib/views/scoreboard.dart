import 'package:flutter/material.dart';
import 'package:flutter_game/models/player.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class Scoreboard extends StatefulWidget {
  const Scoreboard({super.key});

  @override
  State<Scoreboard> createState() => _ScoreboardState();
}

class _ScoreboardState extends State<Scoreboard> {
  @override
  Widget build(BuildContext context) {
    RoomDataProvider roomDataProvider = Provider.of<RoomDataProvider>(context);
    String turnNickname = roomDataProvider.roomData['turn']['nickname'];
    List<Player> players = roomDataProvider.listPlayers;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: players.map((player) => buildPlayer(player, turnNickname)).toList(),
    );
  }

  Widget buildPlayer(Player player, String turnNickname) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SvgPicture.network(
            player.icon,
            height: 30,
            width: 30,
          ),
          Text(
            player.nickname,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: player.nickname == turnNickname ? Colors.green : Colors.grey,
            ),
          ),
          Text(
            player.points.toString(),
            style: const TextStyle(
              fontSize: 25,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
