import 'package:flutter/material.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:provider/provider.dart';

class GameMethods {
  // void checkWinner(BuildContext context, Socket socketClent) {
  //   RoomDataProvider roomDataProvider = Provider.of<RoomDataProvider>(context, listen: false);
  //   final List<String> board = roomDataProvider.board;
  //
  //   String winner = '';
  //
  //   // Checking rows
  //   if (board[0] ==
  //           board[1] &&
  //       board[0] ==
  //           board[2] &&
  //       board[0] != '') {
  //     winner = board[0];
  //   }
  //   if (board[3] ==
  //           board[4] &&
  //       board[3] ==
  //           board[5] &&
  //       board[3] != '') {
  //     winner = board[3];
  //   }
  //   if (board[6] ==
  //           board[7] &&
  //       board[6] ==
  //           board[8] &&
  //       board[6] != '') {
  //     winner = board[6];
  //   }
  //
  //   // Checking Column
  //   if (board[0] ==
  //           board[3] &&
  //       board[0] ==
  //           board[6] &&
  //       board[0] != '') {
  //     winner = board[0];
  //   }
  //   if (board[1] ==
  //           board[4] &&
  //       board[1] ==
  //           board[7] &&
  //       board[1] != '') {
  //     winner = board[1];
  //   }
  //   if (board[2] ==
  //           board[5] &&
  //       board[2] ==
  //           board[8] &&
  //       board[2] != '') {
  //     winner = board[2];
  //   }
  //
  //   // Checking Diagonal
  //   if (board[0] ==
  //           board[4] &&
  //       board[0] ==
  //           board[8] &&
  //       board[0] != '') {
  //     winner = board[0];
  //   }
  //   if (board[2] ==
  //           board[4] &&
  //       board[2] ==
  //           board[6] &&
  //       board[2] != '') {
  //     winner = board[2];
  //   } else if (roomDataProvider.filledBoxes == 9) {
  //     winner = '';
  //     showGameDialog(context, 'Draw');
  //   }
  //
  //   if (winner != '') {
  //     if (roomDataProvider.player1.icon == winner) {
  //       showGameDialog(context, '${roomDataProvider.player1.nickname} won!');
  //       socketClent.emit('winner', {
  //         'winnerSocketId': roomDataProvider.player1.socketID,
  //         'roomId': roomDataProvider.roomData['_id'],
  //       });
  //     } else {
  //       showGameDialog(context, '${roomDataProvider.player2.nickname} won!');
  //       socketClent.emit('winner', {
  //         'winnerSocketId': roomDataProvider.player2.socketID,
  //         'roomId': roomDataProvider.roomData['_id'],
  //       });
  //     }
  //   }
  // }

  void clearBoard(BuildContext context) {
    Provider.of<RoomDataProvider>(context, listen: false).clearBoard();
  }
}
