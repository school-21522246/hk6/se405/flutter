// ignore: library_prefixes
import 'package:socket_io_client/socket_io_client.dart' as IO;

class SocketClient {
  IO.Socket? socket;
  static SocketClient? _instance;
  // String host = 'https://game.uit.asia';
  String host = 'http://localhost:3000';

  SocketClient._internal() {
    socket = IO.io(host, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });
    socket!.connect();
  }

  SocketClient._disconnect() {
    socket?.disconnect();
  }
  static void disconnect() {
    SocketClient._disconnect();
  }

  static SocketClient get instance {
    _instance ??= SocketClient._internal();
    return _instance!;
  }
}
