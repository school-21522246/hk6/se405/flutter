import 'package:flutter/material.dart';
import 'package:flutter_game/provider/room_data_provider.dart';
import 'package:flutter_game/resources/socket_client.dart';
import 'package:flutter_game/screens/game_screen.dart';
import 'package:flutter_game/utils/utils.dart';
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketMethods {
  final _socketClient = SocketClient.instance.socket!;
  RoomDataProvider? roomDataProvider;

  Socket get socketClient => _socketClient;

  void clearListeners() {
    _socketClient.clearListeners();
  }

  // EMITS
  void createRoom(String nickname, String icon) {
    if (nickname.isNotEmpty) {
      _socketClient.emit('createRoom', {
        'nickname': nickname,
        'icon': icon.isNotEmpty ? icon : '',
        'color': 'black',
      });
    }
  }

  void joinRoom(String nickname, String icon, String roomId) {
    if (nickname.isNotEmpty && roomId.isNotEmpty) {
      _socketClient.emit('joinRoom', {
        'nickname': nickname,
        'icon': icon.isNotEmpty ? icon : '',
        'color': 'black',
        'roomId': roomId,
      });
    }
  }

  void tapGrid(int index, String roomId, List<String> board) {
    _socketClient.emit('tap', {
      'index': index,
      'roomId': roomId,
    });
  }

  void leaveRoom({roomId, nickname}) {
    _socketClient.emit('leaveRoom', {
      'roomId': roomId,
      'nickname': nickname,
    });
  }

  // LISTENERS
  void updateRoomListener(BuildContext? context) {
    _socketClient.on('updateRoom', (data) {
      if (context == null) {
        return;
      }
      Provider.of<RoomDataProvider>(context, listen: false)
          .updateRoomData(data['room']);
    });
  }

  void navigateGameScreenListener(BuildContext? context) {
    _socketClient.on('navigateGameScreen', (data) {
      if (context == null) {
        return;
      }
      Navigator.pushNamed(context, GameScreen.routeName);
    });
  }

  void errorOccuredListener(BuildContext? context) {
    _socketClient.on('errorOccurred', (data) {
      if (context == null) {
        return;
      }
      showSnackBar(context, data);
    });
  }

  void tappedListener(BuildContext? context) {
    _socketClient.on('tapped', (data) {
      if (context == null) {
        return;
      }
      Provider.of<RoomDataProvider>(context, listen: false)
          .setBox(data['index'], data['icon']);
    });
  }

  void endGameListener(BuildContext? context) {
    _socketClient.on('endGame', (data) {
      if (context == null) {
        return;
      }
      String title = '';
      String content = '';

      if (data['nickname'] == null) {
        title = 'Draw';
        content = 'The game is a draw';
      } else {
        title = 'Congratulations';
        content = '${data['nickname']} won the game';
      }

      showEndGameDialog(
        context: context,
        title: title,
        content: content,
        icon: data['icon'],
        onPressed: () {
          _socketClient.emit('resetGame', {
            'roomId': data['roomId'],
          });
        },
      );
    });
  }

  void navigateBackListener(BuildContext? context) {
    _socketClient.on('navigateBack', (data) {
      if (context == null) {
        return;
      }
      Navigator.popUntil(context, (route) => route.isFirst);
    });
  }
}
