class Player {
  String nickname;
  String socketID;
  int points;
  String icon;
  String color;
  Player({
    required this.nickname,
    required this.socketID,
    required this.points,
    required this.icon,
    required this.color,
  });

  Map<String, dynamic> toMap() {
    return {
      'nickname': nickname,
      'socketID': socketID,
      'points': points,
      'icon': icon,
      'color': color,
    };
  }

  factory Player.fromMap(Map<String, dynamic> map) {
    return Player(
      nickname: map['nickname'] ?? '',
      socketID: map['socketID'] ?? '',
      points: map['points'] ?? 0,
      icon: map['icon'] ?? '',
      color: map['color'] ?? '',
    );
  }

  Player copyWith({
    String? nickname,
    String? socketID,
    int? points,
    String? icon,
    String? color,
  }) {
    return Player(
      nickname: nickname ?? this.nickname,
      socketID: socketID ?? this.socketID,
      points: points ?? this.points,
      icon: icon ?? this.icon,
      color: color ?? this.color,
    );
  }
}
