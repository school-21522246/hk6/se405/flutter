import 'package:flutter/material.dart';
import 'package:flutter_game/models/player.dart';

class RoomDataProvider extends ChangeNotifier {
  final Map<String, dynamic> _roomData = {};

  int _size = 0;
  final List<String> _board = [];
  int _filledBoxes = 0;
  String _myNickname = '';
  final List<Player> _listPlayers = [];

  Map<String, dynamic> get roomData => _roomData;
  int get size => _size;
  List<String> get board => _board;
  int get filledBoxes => _filledBoxes;
  String get myNickname => _myNickname;
  List<Player> get listPlayers => _listPlayers;

  void updateRoomData(Map<String, dynamic> data) {
    for (String key in data.keys) {
      _roomData[key] = data[key];
    }
    updateBoard(data);
    updateListPlayers(data);

    notifyListeners();
  }

  // void updatePlayer(int index, Map<String, dynamic> playerData) {
  //   _listPlayer[index].nickname = playerData['nickname'];
  //   _listPlayer[index].socketID = playerData['socketID'];
  //   _listPlayer[index].points = playerData['points']?.toDouble();
  //   _listPlayer[index].icon = playerData['icon'];
  //   _listPlayer[index].color = playerData['color'];
  //   notifyListeners();
  // }

  void updateBoard(Map<String, dynamic> data) {
    _size = data['size'] ?? 3;
    _board.clear();
    for (int i = 0; i < _size * _size; i++) {
      _board.add(data['board']?[i] ?? '');
    }
    _filledBoxes = data['filledBoxes'] ?? 0;
    notifyListeners();
  }
  
  void updateListPlayers(Map<String, dynamic> data) {
    List<dynamic> listPlayerData = data['players'] ?? [];

    _listPlayers.clear();
    for (int i = 0; i < listPlayerData.length; i++) {
      _listPlayers.add(Player.fromMap(listPlayerData[i]));
    }
    notifyListeners();
  }

  void setBox(int index, String value) {
    _board[index] = value;
    notifyListeners();
  }

  void clearBoard() {
    _filledBoxes = 0;
    _board.clear();
    for (int i = 0; i < _size * _size; i++) {
      _board.add('');
    }
  }

  void setNickname(String nickname) {
    _myNickname = nickname;
  }
}
